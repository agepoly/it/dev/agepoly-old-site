django==1.6.10
git+https://github.com/agepoly/mezzanine.git
south
pysolr
git+https://github.com/agepoly/django-oscar.git@Branch_0.7.2
git+https://bitbucket.org/naritas/mezzanine-mailchimp.git
pycountry
django-oscar-paypal==0.9.1
raven
git+https://github.com/PolyLAN/polybookexchange.git
django-modeltranslation
git+https://github.com/agepoly/DjangoBB.git
