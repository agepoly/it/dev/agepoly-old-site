from modeltranslation.translator import translator, TranslationOptions
from .models import Team, Homepage, Vignette, CircleNumber

class TeamTranslationOptions(TranslationOptions):
    fields = ('tag_line', 'content')

translator.register(Team, TeamTranslationOptions)

class HomepageTranslationOptions(TranslationOptions):
    fields = ('content', )

translator.register(Homepage, HomepageTranslationOptions)

class VignetteTranslationOptions(TranslationOptions):
    fields = ('title', 'content')

translator.register(Vignette, VignetteTranslationOptions)

class CircleNumberTranslationOptions(TranslationOptions):
    fields = ('title', 'description')

translator.register(CircleNumber, CircleNumberTranslationOptions)
