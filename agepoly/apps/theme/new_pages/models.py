from django.db import models
from mezzanine.pages.models import Page, RichText

from django.core.exceptions import ValidationError

VIGNETTE_COUNT = 6
CIRCLE_NUMBER_COUNT = 4

class PositionValidator(object):

    def __init__(self, total_position):
        self.total_position = total_position

    def __call__(self, value):
        if value is None:
            return
        if type(value) != int:
            raise ValidationError(u"%s is not an integer" % value)
        if value < 1 or value > self.total_position:
            raise ValidationError(u"The position should be between 1 and %s" % (value, self.total_position))


class Team(Page, RichText):
    tag_line = models.CharField(max_length=200)
    cover = models.ImageField(upload_to="teams")

class Vignette(models.Model):
    title = models.CharField(max_length=40)
    # small_logo = models.ImageField(upload_to="logos")
    logo_name = models.CharField(max_length=30)
    content = models.CharField(max_length=150)
    homepage = models.ForeignKey("Homepage")
    position = models.IntegerField(validators=[PositionValidator(VIGNETTE_COUNT)], null=True, unique=True, blank=True)

    class Meta:
        ordering = ['position']

class CircleNumber(models.Model):
    number = models.PositiveIntegerField()
    title = models.CharField(max_length=15)
    description = models.CharField(max_length=75)
    homepage = models.ForeignKey("Homepage")
    position = models.IntegerField(validators=[PositionValidator(CIRCLE_NUMBER_COUNT)], null=True, unique=True, blank=True)

    class Meta:
        ordering = ['position']

class Homepage(Page, RichText):
    def __init__(self, *args, **kwargs):
        super(Homepage, self).__init__(*args, **kwargs)
        self.title = "Homepage"
        # Force the slug to be "/". Configure the url in urls.py to make the
        # homepage be the page with slug "/".
        self.slug = "/"
        # Get an exception if _old_slug isn't set. :/
        self._old_slug = "/"
        # Doesn't show the Homepage in the menu
        self.in_menus = tuple()
