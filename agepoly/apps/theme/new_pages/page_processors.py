from mezzanine.pages.page_processors import processor_for
from .models import Homepage

from django.utils import timezone
from django.core.cache import cache

from mezzanine.core.models import CONTENT_STATUS_PUBLISHED

from mezzanine.blog.models import BlogPost

import requests
import random
from itertools import izip_longest

@processor_for(Homepage)
def add_blog_post(request, page):
    blog_posts = BlogPost.objects.order_by("-publish_date")\
        .filter(status=CONTENT_STATUS_PUBLISHED)\
        .filter(publish_date__lte=timezone.now())[:3]\
        .all()

    return {"latest_blog_posts": blog_posts}

#@processor_for(Homepage)
#def add_facebook_photos(request, page):
#    r = requests.get("https://graph.facebook.com/122046727875595?fields=albums{name,id,created_time}")
#    json_object = r.json()
#
#    data = json_object["albums"]["data"]
#
#    list.sort(data, key=lambda data: data["created_time"], reverse=True)
#
#    photos = cache.get("fb_photos") or []
#
#    if len(photos) == 0:
#        for i in range(3):
#            url = "https://graph.facebook.com/" + data[i]["id"] + "/photos"
#            r = requests.get(url)
#
#            json_object = r.json()
#
#            images = json_object["data"]
#
#            images = filter(lambda img: img["width"] > img["height"], images)
#            nombre_images = len(images) #Facebook always only returns 25 images, but to be sure check anyway
#
#            for index1, index2 in grouper(2, random.sample(range(0, nombre_images), 8)):
#                image_1, image_2 = images[index1], images[index2]
#                to_append = (
#                    (image_1["images"][4]["source"], image_1["link"]),
#                    (image_2["images"][4]["source"], image_2["link"]),
#                )
#
#                photos.append(to_append)
#
#        cache.set("fb_photos", photos, 60*10)
#
#    return {"facebook_photos": photos}

def grouper(n, iterable, fillvalue=None):
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

