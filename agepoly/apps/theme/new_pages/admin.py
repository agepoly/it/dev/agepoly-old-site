from django.contrib import admin
from modeltranslation.admin import TranslationTabularInline
from mezzanine.pages.admin import PageAdmin
from mezzanine.core.admin import SingletonAdmin
from .models import Team, Homepage, Vignette, CircleNumber, VIGNETTE_COUNT, CIRCLE_NUMBER_COUNT

# from copy import deepcopy

# homepage_fieldsets = deepcopy(PageAdmin.fieldsets)
# homepage_fieldsets[0][1]["fields"].remove("status")
# # remove publish_date and expiry_date
# homepage_fieldsets[0][1]["fields"].pop(1)

admin.site.register(Team, PageAdmin)

class VignetteInline(TranslationTabularInline):
    model = Vignette
    extra = VIGNETTE_COUNT
    max_num = VIGNETTE_COUNT
    fields = ('position', 'title', 'logo_name', 'content')


class CircleNumberInline(TranslationTabularInline):
    model = CircleNumber
    extra = CIRCLE_NUMBER_COUNT
    max_num = CIRCLE_NUMBER_COUNT
    fields = ('position', 'title', 'number', 'description')

class HomepageAdmin(PageAdmin, SingletonAdmin):
    inlines = (VignetteInline, CircleNumberInline,)
    fieldsets = ((None, {"fields": ["content"]}),)

admin.site.register(Homepage, HomepageAdmin)
