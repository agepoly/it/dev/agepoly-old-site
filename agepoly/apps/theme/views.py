from django.shortcuts import render
from django.http import HttpResponse

from mezzanine.utils.views import render

import requests

# Create your views here.
def news_item(request, news_item_id):
    
    r = requests.get("http://10.7.0.22/communication/website_news")
    json = r.json()
    
    title = None
    content = None
    url = None
    
    for news_item in json:
        if news_item["id"] == news_item_id:
                title = news_item["title"]
                content = news_item["content"]
                url = news_item["url"]
    
    template = 'pages/news_item.html'
    context = {"title":title, "content":content, "url":url}
    
    return render(request, template, context)