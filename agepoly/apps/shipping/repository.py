from decimal import Decimal as D

from oscar.apps.shipping.methods import Free, FixedPrice
from oscar.apps.shipping import repository, methods as core_methods
from django.utils.translation import ugettext_lazy as _


class Economy(core_methods.FixedPrice):
    code = 'agep_economy'
    name = _("PostPac Economy")
    description = _('Delivered anywhere in Switzerland within two working days (Monday to Friday).')


class Priority(core_methods.FixedPrice):
    code = 'agep_priority'
    name = _("PostPac Priority")
    description = _('Delivered on the next working day (Monday to Friday).')


class InShop(core_methods.Free):
    code = 'agep_in_shop'
    name = _("Pickup at the AGEPoly shop")
    description = _('Pickup at AGEPoly shop on campus (Monday to Friday - 10 A.M. to 4:45 P.M.).')


class Repository(repository.Repository):
    methods = [InShop(), Economy(D('7.00'), D('7.00')), Priority(D('9.00'), D('9.00'))]

    def get_shipping_methods(self, user, basket, shipping_addr=None, **kwargs):
        return self.prime_methods(basket, self.methods)

    def find_by_code(self, code, basket):
        for method in self.methods:
            if code == method.code:
                return self.prime_method(basket, method)
