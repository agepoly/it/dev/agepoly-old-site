
DEBUG = True

# Make these unique, and don't share it with anybody.
SECRET_KEY = "%(secret_key)s"
NEVERCACHE_KEY = "%(secret_key2)s"

DATABASES = {
    "default": {
        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.mysql",
        # DB name or path to database file if using sqlite3.
        "NAME": "agepdev",
        # Not used with sqlite3.
        "USER": "agepdev",
        # Not used with sqlite3.
        "PASSWORD": "%(mysql_password)s",
        # Set to empty string for localhost. Not used with sqlite3.
        "HOST": "mysql",
        # Set to empty string for default. Not used with sqlite3.
        "PORT": "",
    }
}
