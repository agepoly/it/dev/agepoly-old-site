from fabric.api import *
from fabric.contrib.files import upload_template, append, comment

import uuid

import config


@task
def move_www_home():
    """Move home of www-data to /home"""
    sudo("mkdir /home/www-data")
    sudo("chown www-data:www-data /home/www-data")
    sudo("service apache2 stop")
    sudo("usermod -d /home/www-data www-data")
    sudo("service apache2 start")


@task
def install_apache():
    """Install apache"""
    sudo('apt-get -y install apache2 libapache2-mod-wsgi')


@task
def configure_apache():
    """Configure apache"""
    # Disable default site
    sudo('a2dissite 000-default')

    # Copy config
    upload_template('files/apache.conf', '/etc/apache2/sites-available/apache.conf',  {})

    # Enable config
    sudo('a2ensite apache.conf', pty=True)


@task
def restart_apache():
    """Restart apache"""
    sudo('service apache2 restart')


@task
def install_python():
    """Install python and python deps"""
    sudo('apt-get install -y python-crypto python-mysqldb python-imaging python-pip python python-dev libjpeg-dev libxml2-dev libxslt1-dev')


@task
def install_git():
    """Install git"""
    sudo('apt-get install -y git')


@task
def clone_repo():
    """Clone the git repository"""

    sudo('mkdir -p /var/www/git-repo')

    with cd('/var/www/git-repo'):
        sudo('git clone ssh://git@gitlab.dit.polylan.ch:10622/agep-info/agepoly-site.git agepoly')


@task
def pull_repos():
    """Pull the git repository"""

    with cd('/var/www/git-repo/agepoly'):
        sudo('git pull')


@task
def install_pip_dep():
    """Install python depenencies using pip"""

    sudo('pip install -r /var/www/git-repo/agepoly/agepoly/data/pip-reqs.txt --upgrade')


@task
def install_supervisor():
    """Install supervisor"""
    sudo('apt-get install -y supervisor')


@task
def chmod_and_chown():
    """Update folder rights"""
    with cd('/var/www/git-repo'):
        sudo("chown -R www-data:www-data .")

    with cd('/var/log/apache2/'):
        sudo('chown www-data:www-data .')
        sudo('touch django.log')
        sudo("chmod 777 *")


@task
def sync_databases():
    """Sync django databases"""
    with cd('/var/www/git-repo/agepoly/agepoly'):
        sudo("python manage.py syncdb --noinput")
        sudo("python manage.py migrate --noinput")


@task
def collect_statics():
    """Collect statics"""
    with cd('/var/www/git-repo/agepoly/agepoly'):
        sudo("python manage.py collectstatic --noinput")


@task
def configure_agepoly():
    """Configure the djagno application"""
    upload_template('files/settingsLocal.py', '/var/www/git-repo/agepoly/agepoly/app/settingsLocal.py',  {
        'mysql_password': config.MYSQL_PASSWORD,
        'secret_key': str(uuid.uuid4()),
        'secret_key2': str(uuid.uuid4()),
        })


@task
def update_code():
    """Update code"""

    execute(pull_repos)
    execute(install_pip_dep)
    execute(collect_statics)
    execute(chmod_and_chown)
    execute(sync_databases)
    execute(restart_apache)


@task
def quick_update_code():
    """Quick update code"""

    execute(pull_repos)
    execute(install_pip_dep)
    execute(restart_apache)


@task
def deploy_new():
    """Deploy a new server"""

    execute(install_apache)
    execute(move_www_home)
    execute(configure_apache)

    execute(install_git)
    execute(clone_repo)

    execute(install_python)
    execute(install_pip_dep)

    execute(install_supervisor)

    execute(configure_agepoly)

    execute(chmod_and_chown)

    execute(sync_databases)

    execute(restart_apache)
